
var shield;

function drawBoxes(){
		noStroke();
		fill(0);
		for (i=0; i < codex.length; i++) {
		rect(codex[i],codey[i],25,25);
	}
}

var button_new, button_sverdspinn;


var boxes = 0;
var codex = [];
var codey = [];
function setup() {
// colors
	green = color(139, 205, 0);
	darkgreen = color(0, 76, 44);
	white = color(230,240,255);
	black = color(20,20,60);

  button_new = createButton('Nytt skjold');
  button_new.position(19, 19);
  button_new.mousePressed(function(){drawshield(false)});
  
  button_sverdspinn = createButton('Sverdspinn');
  button_sverdspinn.position(19, 200);
  button_sverdspinn.mousePressed(function(){drawshield(true)});
  


  createCanvas(600,600);
  background(50);
  drawcodes();
  drawshield();
  //randomCodes();

}

function randomCodes() {
	// random if we should draw or not
	// random 1
	// left,right:12 rects, top:11 rects
	var left, right = 12
	var top = 11;
	var r;
	
	noStroke();
	strokeWeight(1);
	stroke(black);
	//left
	for (i = 0; i < 12; i++) {
	r = round(random(1));
		if (r == 1) {
			push();
				translate(0,24);
				rect(24, 24*i, 24, 24);
			pop();
		}
	}
	//right -- need new random number
	for (i = 0; i < 12; i++) {

	r = round(random(1));
		if (r == 1) {
			push();
				translate(240,24);
				rect(24, 24*i, 24, 24);
			pop();
		}
	}
	//top -- need new random number
	for (i = 0; i < 11; i++) {

	r = round(random(1));
		if (r == 1) {
			push();
				translate(24,0);
				rect(24*i, 24, 24, 24);
			pop();
		}
	}
}

function newShield(){
	// buttton for drawing a new shield.
}

function storeCode(){
	// store the codes that are reported as working
	
}

// with the size as shown, the grid is 
// left:11, top: 10, right: 11

function drawcodes(){
	fill(white);
	rectWidth = 24;
	background(black);''
}

function qrbackground(){
		// qrbackground
		stroke(white);
		strokeWeight(25);
		
		beginShape();
			vertex(37,299);
			vertex(37,37);
			vertex(275,37);
			vertex(275,299);		
		endShape();

}
function drawshield(sverdspinn){
	fill(white);
	stroke(white);	
	strokeCap('square');
	strokeWeight(24);

	//shield
	push();
	
translate(width/4,50);
	scale(0.94);
		// outer
/*		beginShape();
			vertex(150, 20);
			vertex(450, 20);
			vertex(450, 340);
			vertex(300, 490);
			vertex(150, 340);
		endShape(CLOSE);
*/	beginShape();
			vertex(0, 0);
			vertex(311, 0);
			vertex(311, 320);
			vertex(155.5, 470);
			vertex(0, 320);
		endShape(CLOSE);

		// inner
		stroke(black);
		strokeWeight(39);
/*		beginShape();
			vertex(178, 48); //upperleft
			vertex(422, 48); // upper right
			vertex(422, 328); // lower right
			vertex(300, 450); // point
			vertex(178, 328); // lower left
		endShape(CLOSE);
*/
		beginShape();
			vertex(20, 20); //upperleft
			vertex(291, 20); // upper right
			vertex(291, 311); // lower right
			vertex(155.5, 440); // point
			vertex(20, 311); // lower left
		endShape(CLOSE);

		qrbackground();	

		// code
		noStroke();
		fill(black);
		stroke(black);
		strokeWeight(1);
		
		
		// working: Sverdspinn, dunno the english name for it
		if (sverdspinn) {
			rect(24, 24, 24, 24);
			rect(24, 24*2, 24, 24);
			rect(24, 24*5, 24, 24);
			rect(24, 24*5, 24, 24);
			rect(24, 24*9, 24, 24);
			
			rect(24*2, 24, 24, 24);
			rect(24*5, 24, 24, 24);
			rect(24*7, 24, 24, 24);
			rect(24*8, 24, 24, 24); 
	
			rect(264, 24, 24, 24);
			rect(264, 24*2, 24, 24);
			rect(264, 24*3, 24, 24);
			rect(264, 24*5, 24, 24);
			rect(264, 24*7, 24, 24);
			rect(264, 24*9, 24, 24);
		}	else {	
			randomCodes();	
		}
	pop();
	

}

function touchEnded(){
	//drawshield(false);
	//return(false);
}

function keyTyped() {
  if (key === ' ') {
	drawshield();
  }
  // uncomment to prevent any default behavior
  return false;
}



function draw() {
	//background(black);
	//randomCodes();
	//drawshield();
}